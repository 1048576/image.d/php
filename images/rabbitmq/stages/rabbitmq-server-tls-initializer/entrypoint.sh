#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

source_dirpath="/run/secrets/tls/"
target_dirpath="/etc/rabbitmq/tls/"

csh "cp -L ${source_dirpath}* ${target_dirpath}"
csh "chown rabbitmq:rabbitmq ${target_dirpath}*"
csh "chmod 0600 ${target_dirpath}*"
