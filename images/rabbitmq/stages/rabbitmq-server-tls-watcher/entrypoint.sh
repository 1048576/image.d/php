#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

host="127.0.0.1"
port="5671"
source_dirpath="/run/secrets/tls/"
target_dirpath="/etc/rabbitmq/tls/"

log() {
    jq -cnM --arg timestamp $(date +"%Y-%m-%dT%H:%M:%S%z") --arg message "$1" "\$ARGS.named"
}

wait_for_port_to_open() {
    until nc -z ${host} ${port} 2>&1 > /dev/null; do
        log "Waiting for port to open"

        sleep 10
    done
}

actual_certificate() {
    cat /dev/null | openssl s_client -connect ${host}:${port} 2>/dev/null | openssl x509 -noout -text
}

expected_certificate() {
    openssl x509 -noout -text -in "$1tls.crt"
}

wait_for_port_to_open

log "Running"

while true; do
    wait_for_port_to_open

    if [ "$(actual_certificate)" != "$(expected_certificate ${source_dirpath})" ]; then
        log "Certificate needs to be refreshed"

        cp -L ${source_dirpath}* ${target_dirpath}
        chown rabbitmq:rabbitmq ${target_dirpath}*
        chmod 0600 ${target_dirpath}*

        reload_output=$(rabbitmqctl eval 'ssl:clear_pem_cache().')
        reload_status=$?

        log "Server reload status: ${reload_output}"

        if [ "${reload_status}" != "0" ]; then
            exit 1
        fi

        sleep 60

        wait_for_port_to_open

        if [ "$(actual_certificate)" == "$(expected_certificate ${target_dirpath})" ]; then
            log "Certificate successfully refreshed"
        else
            log "Failed to refresh certificate"

            exit 60
        fi
    else
        log "Certificate refresh is not required"

        sleep 1
    fi
done
