#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

printenv COMPOSER_AUTH_JSON > ${COMPOSER_HOME}/auth.json

csh "/usr/local/bin/composer install"

rm -rf "${COMPOSER_HOME}/*"
