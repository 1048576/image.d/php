#!/usr/bin/env sh

set -o errexit
set -o nounset
set -o pipefail

datadir="/var/lib/postgresql/${POSTGRESQL_CLUSTER_NAME}/"

exec /usr/libexec/postgresql16/postgres -D "${datadir}" $@
